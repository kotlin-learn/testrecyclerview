package com.pakawat.workshop1_loginandactivityintent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pakawat.workshop1_loginandactivityintent.databinding.ActivityMainBinding

class DetailActivity : AppCompatActivity() {

    var mDataArray = arrayListOf<ItemBean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val acc = intent.getParcelableExtra<Account>("account")
        if (acc != null) {
            Toast.makeText(applicationContext,"Detail ${acc.username} ${acc.password}", Toast.LENGTH_LONG).show()
        }

        addDummyData()

         val recyclerView = findViewById<RecyclerView>(R.id.mRecyclerView)
        recyclerView.adapter = CustomAdapter()
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun addDummyData() {
        for (i in 1..100 ){
            if(i%2 ==0 ) {
                mDataArray.add(
                    ItemBean(
                        "Bestza01",
                        "เบสน่ารัก $i",
                        "https://cdn.pixabay.com/photo/2015/04/23/21/59/tree-736875_960_720.jpg"
                    )
                )
            }else{
                mDataArray.add(
                    ItemBean(
                        "Bestza01",
                        "เบสน่ารัก $i",
                        "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg"
                    )
                )
            }
        }
//        findViewById<RecyclerView>(R.id.mRecyclerView).adapter!!.notifyDataSetChanged()
    }

    inner class CustomAdapter : RecyclerView.Adapter<CustomAdapter.CustomHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomHolder {
            val adapterLayout =  LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false)
            return CustomHolder(adapterLayout)
        }


        override fun onBindViewHolder(holder: CustomHolder, position: Int) {
                holder.title.text = mDataArray[position].title
                holder.subtitle.text = mDataArray[position].subtitle
                Glide.with(this@DetailActivity).load(mDataArray[position].image).into(holder.image)
        }


        override fun getItemCount(): Int {
            return mDataArray.size
        }

        inner class CustomHolder(private val itemView: View) : RecyclerView.ViewHolder(itemView) {
            val title = itemView.findViewById<TextView>(R.id.mTitleTextView)
            val subtitle = itemView.findViewById<TextView>(R.id.mSubtitleTextView)
            val image = itemView.findViewById<ImageView>(R.id.mImageView)

        }

    }


}



