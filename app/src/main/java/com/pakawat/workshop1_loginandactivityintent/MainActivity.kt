package com.pakawat.workshop1_loginandactivityintent

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.pakawat.workshop1_loginandactivityintent.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    fun onClickLogin(view: View) {
        val userName = binding.mUsername.text.toString()
        val password = binding.mPassword.text.toString()
        //i เหมือนข้อความที่ใช้ในการส่งข้อมูลว่าจะเปิดหน้าไหน
        val i = Intent(this,DetailActivity::class.java) //context,ปลายทาง
        //i.putExtra("username",userName)
        //i.putExtra("password",password)
        var acc = Account(userName,password)
        i.putExtra("account",acc)
        startActivity(i)

        Toast.makeText(applicationContext,"Login $userName $password",Toast.LENGTH_LONG).show()
    }
    fun onClickRegister(view: View) {
        Toast.makeText(applicationContext,"Register",Toast.LENGTH_LONG).show()
    }
}